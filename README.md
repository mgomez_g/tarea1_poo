# Tarea 1 ELO329
### Integrantes 
* Gabriela Bustamante 
* Marcelo Fernandez
* Mariapaz Gómez
* Julio Maturana

### Uso de Makefile

- **Compilación**
  > `make`
- **Ejecución y archivo de salida**
    > `make run > Salida.csv`
- **Eliminar .Class**
  > `make clean` 

### Uso de javac y java en terminal

- **Compilación**
  > `javac Lamp.java Cloud.java LampControl.java LampState.java Operator.java Stage1.java`

- **Ejecución con archivo de salida**
  > `java Stage1 configurationFile.txt > Salida.csv`
