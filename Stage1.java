import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Stage1 {
    public static void main(String [] args) throws IOException {
        if (args.length != 1) {
            System.out.println("Usage: java Stage1 <configurationFile.txt>");
            System.exit(-1);
        }
        Scanner in = new Scanner(new File(args[0]));
        //System.out.println("File: " + args[0]);
        Cloud cloud = new Cloud(); // creamos la nube

        // reading <#_de_cortinas> <#_de_lámparas> <#_controles_cortinas> <#_controles_lámparas>
        in.nextInt();  // skip number of roller shades / saltamos el numero de cortinas

        int numLamps = in.nextInt(); // capturamos numero de lamparas

        in.nextInt();  // skip number of roller shade controls / salta el numero de controles para cortinas

        int numLampsControl = in.nextInt(); // capturamos numero de controles para lamparas

        in.nextLine(); // skipping <alfa0> <length0> <canal0> … <alfaN_1> <lengthN_1> <canalN_1>
        // saltamos las config. de las cortinas

        // creating lamps according to <canal0>…..<canalL_1> for just one lamp

        // capturamos y asignamos los canales de las lampras
        int channel = in.nextInt();  // esto equivale a la configuracion de las lamparas
        Lamp lamp = new Lamp(channel);
        cloud.addLamp(lamp); // agregamos la primera lampara al arreglo de lamparas }

        // skipping creation of roller shade's controls at <canal0> / saltamos la configuracion de controles de cortinas
        in.nextLine();
        // creating just one lamp's control at <canal0>

        channel = in.nextInt(); // configuracion de controles para las lampras
        LampControl lampControl = new LampControl(channel, cloud);
        Operator operator = new Operator(lampControl, cloud);
        operator.executeCommands(in, System.out);
    }
}
