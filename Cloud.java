import java.util.ArrayList;

public class Cloud {
    public Cloud() {
        lamps = new ArrayList<Lamp>();
    }
    public void addLamp(Lamp l){
        lamps.add(l);
    }
    public Lamp getLampAtChannel( int channel){
        //???
        // suponiendo que tenemos solo una lampara
        for(Lamp l: lamps) {
            if (l.getChannel() == channel) {
                //System.out.println("lampara" + l.getHeader() + "esta en el canal" + channel);
                return l;
            }
        }
        return null;
    }

    public void changeLampPowerState(int channel){
        //??
        lamps.get(0).changePowerState();

    }
    public String getHeaders(){
        String header = "";
        for (Lamp l: lamps)
            header += l.getHeader();
        return header;
    }
    public String getState(){
        //??

        String state = "";
        for(Lamp l: lamps){
            state += l.toString();

        }
        return state;
    }
    private ArrayList<Lamp> lamps; // getting ready for next stages
}
