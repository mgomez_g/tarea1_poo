JFLAGS = -g -encoding ISO8859_1
JC = javac
JVM= java
ARGS= configurationFile.txt
Salida= Salida.csv
.SUFFIXES: .java .class
.java.class:
	$(JC) $(JFLAGS) $*.java

CLASSES = Lamp.java Cloud.java LampControl.java LampState.java Operator.java Stage1.java

MAIN = Stage1

default: classes

classes: $(CLASSES:.java=.class)

run:
	$(JVM) $(MAIN) $(ARGS) > $(Salida)

clean:
	$(RM) *.class